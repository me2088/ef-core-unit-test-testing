﻿// See https://aka.ms/new-console-template for more information

using EfCoreUnitTesting.Example;
using EfCoreUnitTesting.Example.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

// Dependency injection setup and app configuration
var serviceProvider = new ServiceCollection()
    .AddDbContext<LibraryContext>(options => options.UseInMemoryDatabase("EfCoreUnitTesting"))
    .AddScoped<IBookRepository, BookRepository>()
    .AddScoped<Library>()
    .BuildServiceProvider();

var library = serviceProvider.GetRequiredService<Library>();

// Various operations for testing, add books, get books, checkout books, etc.
library.AddBook("The Hobbit");
library.AddBook("The Lord of the Rings");

var books = library.GetBooks();
foreach (var book in books)
{
    Console.WriteLine(book.Title);
}

library.CheckoutBook("The Hobbit");
books = library.GetBooks();
foreach (var book in books)
{
    Console.WriteLine(book.Title);
}

library.CheckinBook("The Hobbit");
books = library.GetBooks();
foreach (var book in books)
{
    Console.WriteLine(book.Title);
}