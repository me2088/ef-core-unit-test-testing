﻿using EfCoreUnitTesting.Example;
using EfCoreUnitTesting.Example.Models;
using EfCoreUnitTesting.Example.Repositories;
using EfCoreUnitTesting.Example.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;

namespace ExampleTests;

public class AlternateCheckoutUnitTests
{
    [Fact]
    public async Task CheckoutBook_UpdatesBookStatusAndRecordsHistory()
    {
        var book = new Book { BookId = Guid.NewGuid(), IsCheckedOut = false };
        var libraryContextMock = new Mock<LibraryContext>();
        var bookDb = new Mock<DbSet<Book>>();
        // bookDb.Object.Add(book);
        IList<Book> books = new List<Book>(){book};
        // libraryContextMock.Setup(x => x.Books).ReturnsDbSet(bookDb.Object);
        libraryContextMock.Setup(x => x.Books).ReturnsDbSet(books);
        // Arrange
        // var mockCheckoutHistoryRepository = new Mock<ICheckoutHistoryRepository>();
        var checkoutService = new AlternateCheckoutService(libraryContextMock.Object);

        // Act
        var success = await checkoutService.CheckoutBook(book);

        // Assert if the book 
        Assert.True(success);
        Assert.True(book.IsCheckedOut);
        libraryContextMock.Verify(x => x.Books.Update(It.Is<Book>(y => y == book)), Times.Once);
        libraryContextMock.Verify(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        
        
    }
}