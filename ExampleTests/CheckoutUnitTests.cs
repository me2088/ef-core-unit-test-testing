using EfCoreUnitTesting.Example.Models;
using EfCoreUnitTesting.Example.Repositories;
using EfCoreUnitTesting.Example.Services;
using Moq;

namespace ExampleTests;

public class CheckoutUnitTests
{
    [Fact]
    public async Task CheckoutBook_UpdatesBookStatusAndRecordsHistory()
    {
        // Arrange
        var book = new Book { BookId = Guid.NewGuid(), IsCheckedOut = false };
        var mockBookRepository = new Mock<IBookRepository>();
        // var mockCheckoutHistoryRepository = new Mock<ICheckoutHistoryRepository>();

        mockBookRepository.Setup(r => r.GetBookAsync(book.BookId)).ReturnsAsync(book);
        mockBookRepository.Setup(r => r.UpdateAsync(book)).Returns(Task.CompletedTask);
        // mockCheckoutHistoryRepository.Setup(r => r.AddRecordAsync(It.IsAny<CheckoutHistory>())).Returns(Task.CompletedTask);

        var checkoutService = new CheckoutService(mockBookRepository.Object);

        // Act
        var success = await checkoutService.CheckoutBook(book);

        // Assert
        Assert.True(success);
        Assert.True(book.IsCheckedOut);
        mockBookRepository.Verify(r => r.UpdateAsync(book), Times.Once);
        // mockCheckoutHistoryRepository.Verify(r => r.AddRecordAsync(It.IsAny<CheckoutHistory>()), Times.Once);
    }
}