﻿using EfCoreUnitTesting.Example.Models;

namespace EfCoreUnitTesting.Example.Services;

public class AlternateCheckoutService
{
   private LibraryContext _libraryContext;
   public AlternateCheckoutService(LibraryContext libraryContext)
   {
         _libraryContext = libraryContext;
   } 
   
    public async Task<bool> CheckoutBook(Book book)
    {
         book.IsCheckedOut = true;
         _libraryContext.Books.Update(book);
         await _libraryContext.SaveChangesAsync();
         return true;
    }
   
}