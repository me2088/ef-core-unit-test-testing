﻿using EfCoreUnitTesting.Example.Models;
using EfCoreUnitTesting.Example.Repositories;

namespace EfCoreUnitTesting.Example.Services;

public class CheckoutService
{
    private IBookRepository _bookRepository;
    public CheckoutService(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
    }
    public async Task<bool> CheckoutBook(Book book)
    {
        book.IsCheckedOut = true;
        await _bookRepository.UpdateAsync(book);
        return true;
    }
}