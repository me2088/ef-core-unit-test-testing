﻿using EfCoreUnitTesting.Example.Models;
using EfCoreUnitTesting.Example.Repositories;
using EfCoreUnitTesting.Example.Services;

namespace EfCoreUnitTesting.Example;

public class Library
{
    private CheckoutService _checkoutService;
    private BookRepository _bookRepository;
    public Library(CheckoutService checkoutService, BookRepository bookRepository)
    {
        _checkoutService = checkoutService;
        _bookRepository = bookRepository;
    }
    
    public async Task<bool> CheckoutBook(Guid bookId)
    {
        var book = await _bookRepository.GetBookAsync(bookId);
        if (book == null)
        {
            throw new Exception("Book not found");
        }
        
        return await _checkoutService.CheckoutBook(book);
    }
}