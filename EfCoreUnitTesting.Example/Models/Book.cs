﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfCoreUnitTesting.Example.Models;

[Table("Book")]
public class Book
{
    [Key]
    public Guid BookId { get; set; }
    [MaxLength(200)]
    public string Title { get; set; } = null!;
    [DefaultValue(false)]
    public bool IsCheckedOut { get; set; }
}