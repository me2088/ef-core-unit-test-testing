﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EfCoreUnitTesting.Example.Models;

[Table("Book")]
public class Author 
{
    [Key]
    public Guid BookId { get; set; }
    [MaxLength(200)]
    public string Title { get; set; } = null!;
}
