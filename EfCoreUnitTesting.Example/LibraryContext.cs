﻿using EfCoreUnitTesting.Example.Models;
using Microsoft.EntityFrameworkCore;

namespace EfCoreUnitTesting.Example;

public class LibraryContext: DbContext
{
    // simple db context with dbsets for books
    public virtual DbSet<Book> Books { get; set; }
    public virtual DbSet<Author> Authors { get; set; }
}
