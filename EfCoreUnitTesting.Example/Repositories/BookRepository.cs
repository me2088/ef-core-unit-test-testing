﻿using EfCoreUnitTesting.Example.Models;
using Microsoft.EntityFrameworkCore;

namespace EfCoreUnitTesting.Example.Repositories;

public interface IBookRepository
{
   Task<Book?> GetBookAsync(Guid bookId);
   Task UpdateAsync(Book book);
}

public class BookRepository : IBookRepository
{
   private LibraryContext _context;
   public BookRepository(LibraryContext context)
   {
      _context = context;
   }

   public async Task<Book?> GetBookAsync(Guid bookId)
   {
      return await _context.Books.FirstOrDefaultAsync(x => x.BookId == bookId);
   }
   
   // update book
   public async Task UpdateAsync(Book book)
   {
      _context.Books.Update(book);
      await _context.SaveChangesAsync();
   }
   
}